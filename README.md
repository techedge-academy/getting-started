**Academy day 1 - Installation Guide**


This guide is focused on installing the tools necessary for the workshops of the week.
To create account use @techedgegroup.com address

Before start anything, join the hangout group using this link:
https://hangouts.google.com/group/eoefZcuzKWUAGfEE2


****
**VISUAL STUDIO CODE**

https://code.visualstudio.com/

****
**POSTMAN**

https://www.getpostman.com/postman

****
**GIT**

https://git-scm.com/downloads

To use git with proxy:

**git config --global http.proxy 

http://proxyuser:proxypwd@proxy.server.com:8080**

****
**NODE JS & NPM**

https://nodejs.org/en/download/

****
**GITLAB - create account**

https://gitlab.com/users/sign_in

****
**APIGEE - create account**

https://apigee.com/api-management/#/homepage

****
**SCP Sap Cloud Platform - create account**

https://cloudplatform.sap.com/index.html

****

**CLOUD FOUNDRY CLI - install on local environment**

https://github.com/cloudfoundry/cli#downloads

****

**BuildMe**

https://www.build.me/

****
